﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncTasksTutorials
{
    public class AsyncEmailClient
    {
        public void processEmail()
        {
            Task<string> getReceiverDetailsTask = getReceiverDetails();
            Task<string> getEmailContentTask = getEmailContent();
            Task<string> getEmailExchangeServerDetailsTask = getEmailExchangeServerDetails();

            //string emailContent = 
                getEmailContentTask.Wait();
            //string emailExchangeServerDetails = 
                getEmailExchangeServerDetailsTask.Wait();
            //string receiverDetails = 
                getReceiverDetailsTask.Wait();
            sendEmail();
        }
        private async Task<string> getEmailContent()
        {
            await Task.Delay(1000);
            return "";
        }
        private async Task<string> getEmailExchangeServerDetails()
        {
            await Task.Delay(1000);
            return "";
        }
        private async Task<string> getReceiverDetails()
        {
            await Task.Delay(1000);
            return "";
        }
        private void sendEmail()
        {
            Thread.Sleep(7000);
        }
    }
    public class SyncEmailClient
    {
        public void processEmail()
        {
            string receiverDetails = getReceiverDetails();
            string emailExchangeServerDetails = getEmailExchangeServerDetails();
            string emailContent = getEmailContent();
            sendEmail();
        }
        private string getEmailContent()
        {
            Thread.Sleep(1000);
            return "";
        }
        private string getEmailExchangeServerDetails()
        {
            Thread.Sleep(1000);
            return "";
        }
        private string getReceiverDetails()
        {
            Thread.Sleep(1000);
            return "";
        }
        private void sendEmail()
        {
            Thread.Sleep(7000);
        }
    }
}
