﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncTasksTutorials
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();

            #region SyncCall

            SyncEmailClient syncEmailClient = new SyncEmailClient();
            
            stopwatch.Start();
            syncEmailClient.processEmail();
            stopwatch.Stop();

            Console.WriteLine("Sync Call Took Time :: " + stopwatch.Elapsed);

            #endregion

            #region AsyncCall

            AsyncEmailClient asyncEmailClient = new AsyncEmailClient();
            
            stopwatch.Restart();
            asyncEmailClient.processEmail();
            stopwatch.Stop();

            Console.WriteLine("Async Call Took Time :: " + stopwatch.Elapsed);

            #endregion

            Console.ReadKey();
        }

    }
}
